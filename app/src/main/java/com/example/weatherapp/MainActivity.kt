package com.example.weatherapp

import android.os.AsyncTask
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_main.*
import org.json.JSONObject
import java.net.URL
import java.text.SimpleDateFormat
import java.util.*

class MainActivity : AppCompatActivity() {

    var CITY: String = "Gliwice"
    val API: String = "402b4dea1b1dc1161cd9bc3dbf11a4c8"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        weatherTask().execute()


    }

    override fun onResume() {
        super.onResume()
        ButtonPrognozuj.setOnClickListener {
            CITY =TextViewNazwaMiasta.getText().toString().toLowerCase()

            weatherTask().execute()
        }
    }

    inner class weatherTask() : AsyncTask<String, Void, String>() {
        override fun onPreExecute() {
            super.onPreExecute()
            /* Showing the ProgressBar, Making the main design GONE */
            findViewById<ProgressBar>(R.id.loader).visibility = View.VISIBLE
            findViewById<ConstraintLayout>(R.id.ConstraintLayoutTresc).visibility = View.GONE
            findViewById<TextView>(R.id.TextVIewInformacja).visibility = View.GONE
        }

        override fun doInBackground(vararg params: String?): String? {
            var response:String?
            try{
                response = URL("https://api.openweathermap.org/data/2.5/weather?q=$CITY&units=metric&appid=$API").readText(
                    Charsets.UTF_8
                )
            }catch (e: Exception){
                response = null
            }
            return response
        }

        override fun onPostExecute(result: String?) {
            super.onPostExecute(result)
            try {
                /* Extracting JSON returns from the API */
                val jsonObj = JSONObject(result)
                val main = jsonObj.getJSONObject("main")
                val sys = jsonObj.getJSONObject("sys")
                val weather = jsonObj.getJSONArray("weather").getJSONObject(0)

                val updatedAt:Long = jsonObj.getLong("dt")
                val updatedAtText = SimpleDateFormat("dd.MM.yyyy hh:mm a ").format(Date(updatedAt*1000))
                val temp = main.getString("temp")+"°C"
                val pressure = main.getString("pressure")+" hPa"

                val sunrise:Long = sys.getLong("sunrise")
                val sunset:Long = sys.getLong("sunset")
                val weatherDescription = weather.getString("description")

                val icon = weather.getString("icon")
                val iconURL = "http://openweathermap.org/img/w/" + icon + ".png"
                Picasso.with(this@MainActivity).load(iconURL).resize(100, 100).into(findViewById<ImageView>(R.id.imageViewIkonka))


                val address = jsonObj.getString("name")

                /* Populating extracted data into our views */
                findViewById<TextView>(R.id.TextViewTytul).text = address
                findViewById<TextView>(R.id.TextViewData).text =  updatedAtText
                findViewById<TextView>(R.id.TextViewOpis).text = weatherDescription.capitalize()

                findViewById<TextView>(R.id.TextViewTemperatura).text = temp


                findViewById<TextView>(R.id.TextViewWschod).text = SimpleDateFormat("hh:mm a").format(Date(sunrise*1000))
                findViewById<TextView>(R.id.TextViewZachod).text = SimpleDateFormat("hh:mm a").format(Date(sunset*1000))
                findViewById<TextView>(R.id.TextViewCisnienie).text = pressure

                /* Views populated, Hiding the loader, Showing the main design */
                findViewById<ProgressBar>(R.id.loader).visibility = View.GONE
                findViewById<ConstraintLayout>(R.id.ConstraintLayoutTresc).visibility = View.VISIBLE

            } catch (e: Exception) {
                findViewById<ProgressBar>(R.id.loader).visibility = View.GONE
                findViewById<TextView>(R.id.TextVIewInformacja).visibility = View.VISIBLE
            }

        }
    }
}